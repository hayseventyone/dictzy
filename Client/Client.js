//Dic Class :
class Dict {
	constructor(title, word, meaning) {
		this.title = title;
		this.word = word;
		this.meaning = meaning;
	}

}
//UI Class: handle UI
class UI {
	static displayDict() {
		const Dictz = myStore.getMyDict();

		Dictz.forEach((dic) => UI.addDicToList(dic));
	}
	static addDicToList(dic) {
		const list = document.querySelector("#Dic-List");

		const row = document.createElement('tr');

		row.innerHTML = `
		<td>${dic.title}</td>
		<td>${dic.word}</td>
		<td>${dic.meaning}</td>
		<td><a href="#" id='del' class="btn-floating btn waves-effect waves-light red"><i class="material-icons">clear</i></a>
		</td>
		`;
		list.appendChild(row);

	}
	static clearField() {
	
		document.querySelector("#word").value = '';
		document.querySelector("#meaning").value = '';

	}
	static deletefromList(element) { //now element class = icon
		if (element.textContent === ('clear')) {
			element.parentElement.parentElement.parentElement.remove(); //up 3 level to <td>

		}


	}
	static showAlert(msg, className) {

		M.toast({
			html: msg,
			classes: className
		}, 2000);

	}
}

//store Class //#endregion
class myStore {
	static getMyDict() {
		let dicts;
		if (localStorage.getItem('dicts') === null)
			dicts = [];
		else
			dicts = JSON.parse(localStorage.getItem('dicts'));
		return dicts;

	}
	static addDict(dict) {
		const dicts = myStore.getMyDict();

		dicts.push(dict);
		localStorage.setItem('dicts', JSON.stringify(dicts));


	}

	static removeDict(word) {

		const dicts = myStore.getMyDict();

		dicts.forEach((dict,idx) => {
			if (dict.word === word){
				dicts.splice(idx,1);
	
			}

		});  
		
		localStorage.setItem('dicts',JSON.stringify(dicts));
	}

}



//Event : Display (getfromStrore)
document.addEventListener('DOMContentLoaded', UI.displayDict);


//Event : Add (NewWord)
document.querySelector("#form-input").addEventListener('submit',
	(event) => {

		event.preventDefault();

		//get form <i class="fas fa-value-absolute    "></i>
		const title = document.querySelector('#title').value;
		const word = document.querySelector('#word').value;
		const meaning = document.querySelector('#meaning').value;


		const InvalidateData = (dict) => (dict.title === '' || dict.word === '' || dict.meaning === '') ? true : false;
		//crate Instant
		const dict = new Dict(title, word, meaning);


		if (InvalidateData(dict)) {
			UI.showAlert('<span >Invalided Data      </span>', ' red')
		} else {
			//Append List UI
			UI.addDicToList(dict);
			//Add to store;
			  myStore.addDict(dict);

			//clear Input with out title//
			UI.clearField();

			UI.showAlert('<span >Success      </span>', 'green')
		}
	});

//Event : Remove(Del)
document.querySelector("#Dic-List").addEventListener('click',
	(evt) => {
        //remove from UI
		UI.deletefromList(evt.target);

		//remove from Store
		myStore.removeDict(evt.target.parentElement.parentElement.previousElementSibling.previousElementSibling.textContent);//find to word

		UI.showAlert('<span >Remove      </span>', 'orange')

	});